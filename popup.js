//показать поле ввода для новой группы--------------------------------------------
function showGrIn(){
	grbl = document.getElementById("gr-bl-in");
	if (grbl.style.display=="block"){
		/*grbl.style.width="0px";*/
		grbl.style.opacity ="0";
		setTimeout(function (){grbl.style.display="none";},150);
	}else{
		grbl.style.display="block";
		setTimeout(function (){grbl.style.opacity ="1";},150);
		
		closeGrTaskPanel();//закроем другие панели
	}
}

//показать список задач группы
function showGrTask(){
	var grid = this.id.split("-")[1];
	
	if (document.getElementById("gr_task-"+grid).style.display=="block"){
		document.getElementById("gr_task-"+grid).style.display="none";
		//document.getElementById("show_task-"+grid).src="arrowd.png";
		//document.getElementById("show_task-"+grid).value=0;
		localStorage.setItem("grstatus"+grid,"0");
	}else{
		document.getElementById("gr_task-"+grid).style.display="block";
		//document.getElementById("show_task-"+grid).src="arrowu.png";
		//document.getElementById("show_task-"+grid).value=1;
		localStorage.setItem("grstatus"+grid,"1");
	}
	//var id = this.id.split("-")[1];
	//saveGroup(id);
	//localStorage.setItem("grstatus"+grid,document.getElementById("show_task-"+grid).value);
}
//показать панель добавления кнопок
function showGrTaskPanel(){
	//pr_grbtn-0
	var grid = this.id.split("-")[1];
	if (document.getElementById("grbtn-"+grid).style.display=="block"){
		document.getElementById("grbtn-"+grid).style.opacity="0.1";
		setTimeout(function(){document.getElementById("grbtn-"+grid).style.display="none";},150);
	}else{
		document.getElementById("grbtn-"+grid).style.display="block";
		setTimeout(function(){document.getElementById("grbtn-"+grid).style.opacity="1";},150);
		document.getElementById("gr-bl-in").style.display="none";//закроем главную панель
	}
}

//скрыть поля ввода создания новой задачи перейти в режим правки группы
function changeGrEdit(grid){
	if (document.getElementById("pr_name-"+grid).disabled){//переключим видимость
		document.getElementById("panelAddTask-"+grid).style.opacity="0";
		setTimeout(function (){document.getElementById("panelAddTask-"+grid).style.display="none";},150);
		document.getElementById("pr_name-"+grid).disabled=false;
		document.getElementById("pr_name-"+grid).style.background="white";
		document.getElementById("pr_name-"+grid).placeholder = document.getElementById("pr_name-"+grid).value;
		document.getElementById("cPrName-"+grid).removeEventListener("click",showGrTask,false);//отключим показ списка по клику, чтобы при вводе поля задачи не моргали
		//покажем кнопки выбора цвета и отмены
		document.getElementById("change-"+grid).style.display="inline-block";
		setTimeout(function (){document.getElementById("change-"+grid).style.opacity="1";},150);
		//уберем возможность сворачивать панель через кнопук меню
		document.getElementById("show_add_panel-"+grid).removeEventListener("click",showGrTaskPanel,false);
		document.getElementById("show_add_panel-"+grid).style.background="none";
	}else{
		//скроем кнопки выбора цвета и отмены
		document.getElementById("change-"+grid).style.opacity="0";
		setTimeout(function (){document.getElementById("change-"+grid).style.display="none";},150);
		//покажем остальные кнопки
		setTimeout(function (){document.getElementById("panelAddTask-"+grid).style.display="inline-block";},150);
		setTimeout(function (){document.getElementById("panelAddTask-"+grid).style.opacity="1";},350);
		document.getElementById("pr_name-"+grid).disabled=true;
		
		document.getElementById("pr_name-"+grid).style.background="none";
		document.getElementById("cPrName-"+grid).addEventListener("click",showGrTask,false);
		//вернем возможность сворачивать панель через кнопук меню
		document.getElementById("show_add_panel-"+grid).addEventListener("click",showGrTaskPanel,false);
		document.getElementById("show_add_panel-"+grid).style.background="#fafeff";
		
	}
}
function showGrEdit(){
	var grid = this.id.split("-")[1];
	changeGrEdit(grid);
}
function undoGrEdit(){
	var grid = this.id.split("-")[1];
	document.getElementById("pr_name-"+grid).value = document.getElementById("pr_name-"+grid).placeholder;
	document.getElementById("pr-"+grid).style.background = document.getElementById("pr-"+grid).getAttribute("oldcolor");
	changeGrEdit(grid);
}

//принять изменения внесенные в группу
function acceptGrEdit(){
	var grid = this.id.split("-")[1];
	document.getElementById("pr_name-"+grid).placeholder = document.getElementById("pr_name-"+grid).value;
	saveGroup(grid);
	changeGrEdit(grid);
}

//применить цвет к текущему полю группы------------------------------------------------------
function setColorCurGr(){
	var grid = this.id.split("-")[1];
	document.getElementById("pr-"+grid).style.background=this.getAttribute("clr");
}

//закрыть панельки всех групп
function closeGrTaskPanel(){
	document.getElementById("grbtn-0").style.display="none";
}

//применить цвет к полю ввода новой группы -----------------------------------------
function setColorGr(){
	document.getElementById("gr-bl-in").style.backgroundColor=this.getAttribute("clr");
}

//добавление новой группы
function addGr(){
	var grid = getGrId();
	if (document.getElementById("proj").value==""){
		var grname = "Project "+grid;
	}else{
		var grname = document.getElementById("proj").value;
	}
	var color = document.getElementById("gr-bl-in").style.background;
	createGr(grid, grname, color, "1");
	saveGroup(grid);
	//закроем окно ввода
	showGrIn();
	//обновим список групп
	if (localStorage.getItem("listGr")){
		localStorage.setItem("listGr",localStorage.getItem("listGr")+grid+"|");
	}else{
		localStorage.setItem("listGr",grid+"|");
	}
	//сборсим ранее введеное значение в поле
	localStorage.removeItem("proj");
	document.getElementById("proj").value = "";
}

//создать группу
function createGr(grid, grname, color, status){
	var src = document.getElementById("pr-0");
	var cln = src.cloneNode(true);
	cln.style.background = color;//
	
	cln.style.display="inline-block";
	//cln.setAttribute("id","pr-"+grid);
	document.getElementById("main").appendChild(cln);
	//Изменим все названия нового проекта
	var elements = "pr|btnDrgn|show_add_panel|cPrName|pr_name|sumtaskgr|sumtgr|grbtn|nametask|namelink|change|acceptgr|undogr|addtask|dropgr|editgr|rstgr|exportgr|colorAgr|colorBgr|colorCgr|colorDgr|colorEgr|colorFgr|gr_task|exportCSVgr|panelAddTask|statusTask|statusA|statusB|statusC|statusD|statusE|statusF|";
	//colorgr;taskaddgr;gr_inf
	var listElements = elements.split("|");
	for (i=0;i<listElements.length-1;i++){
		changeAtGr(listElements[i],grid);
	}
	document.getElementById("pr_name-"+grid).value = grname;
	//показать список задач
	//document.getElementById("show_task-"+grid).addEventListener("click",showGrTask,false);
	document.getElementById("cPrName-"+grid).addEventListener("click",showGrTask,false);//показать список задач группы
	//показать панель добавления задачи
	document.getElementById("show_add_panel-"+grid).addEventListener("click",showGrTaskPanel,false);
	document.getElementById("editgr-"+grid).addEventListener("click",showGrEdit,false);
	document.getElementById("undogr-"+grid).addEventListener("click",undoGrEdit,false);
	document.getElementById("acceptgr-"+grid).addEventListener("click",acceptGrEdit,false);
	//выбор цвета текущей группы
	document.getElementById("colorAgr-"+grid).addEventListener("click",setColorCurGr,false);
	document.getElementById("colorBgr-"+grid).addEventListener("click",setColorCurGr,false);
	document.getElementById("colorCgr-"+grid).addEventListener("click",setColorCurGr,false);
	document.getElementById("colorDgr-"+grid).addEventListener("click",setColorCurGr,false);
	document.getElementById("colorEgr-"+grid).addEventListener("click",setColorCurGr,false);
	document.getElementById("colorFgr-"+grid).addEventListener("click",setColorCurGr,false);
	//выбор статуса для новой задачи
	document.getElementById("statusA-"+grid).addEventListener("click",setStatusTask,false);
	document.getElementById("statusB-"+grid).addEventListener("click",setStatusTask,false);
	document.getElementById("statusC-"+grid).addEventListener("click",setStatusTask,false);
	document.getElementById("statusD-"+grid).addEventListener("click",setStatusTask,false);
	document.getElementById("statusE-"+grid).addEventListener("click",setStatusTask,false);
	document.getElementById("statusF-"+grid).addEventListener("click",setStatusTask,false);
	//добавление задачи
	document.getElementById("addtask-"+grid).addEventListener("click",addTask,false);	
	//удаление задачи
	document.getElementById("dropgr-"+grid).addEventListener("click",removeGr,false);
	//сброс таймеров задач
	document.getElementById("rstgr-"+grid).addEventListener("click",resetTimeGr,false);
	//экспорт задачи
	document.getElementById("exportgr-"+grid).addEventListener("click",exportCSVgr,false);
	//сохраним значение введеных полей на случай сворачивания
	document.getElementById("nametask-"+grid).addEventListener("keyup",saveInput,false);
	document.getElementById("namelink-"+grid).addEventListener("keyup",saveInput,false);
	//развернем список задач, если был развернут ранее
	if (status=="1"){
		document.getElementById("cPrName-"+grid).click();
	}
	//добавим функцию драгдропа
	//document.getElementById("btnDrgn-"+grid).addEventListener("drag",beginDrag,false);
}
//функция драгдроп - захват элемента
function beginDrag(){
	console.log(this);
}
//вычислим разность времени до сохранения 
function diffTime(){
	if (localStorage.getItem("timestamp")){
		var date = new Date();
		var current = Math.round(date.getTime()/1000);
		var olddate = localStorage.getItem("timestamp");
		return (current-parseInt(olddate));		
	}
}

//добавить разницу во времени
function plusTime(oldtime,diff){
	return SecToFtime(parseInt(FtimeToSec(oldtime))+ parseInt(diff));
}

//прочитать из истории все группы
function restoreGr(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var diff = diffTime();
		for (var i=0;i<listGr.length-1;i++){
			//console.log(listGr);
			createGr(
				listGr[i],
				localStorage.getItem("grtitle"+listGr[i]),
				localStorage.getItem("grcolor"+listGr[i]),
				localStorage.getItem("grstatus"+listGr[i])
				);
			//восстановим значение полей если вводили ранее
			if (localStorage.getItem("nametask-"+listGr[i])){
				document.getElementById("nametask-"+listGr[i]).value = localStorage.getItem("nametask-"+listGr[i]);
			}
			if (localStorage.getItem("namelink-"+listGr[i])){
				document.getElementById("namelink-"+listGr[i]).value = localStorage.getItem("namelink-"+listGr[i]);
			}
				
				
			if (localStorage.getItem("listTask"+listGr[i])){
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				var timeTask = "0";
				for (var j=0; j<listTask.length-1; j++){
					if(localStorage.getItem("status"+listGr[i]+"."+listTask[j])=="1"){
							timeTask = plusTime(localStorage.getItem("time"+listGr[i]+"."+listTask[j]),diff);
						}else{
							timeTask = localStorage.getItem("time"+listGr[i]+"."+listTask[j]);
						}
					
					createTask(
						listGr[i],
						listTask[j],
						localStorage.getItem("task"+listGr[i]+"."+listTask[j]),
						localStorage.getItem("link"+listGr[i]+"."+listTask[j]),
						timeTask,
						localStorage.getItem("status"+listGr[i]+"."+listTask[j]),
						localStorage.getItem("type"+listGr[i]+"."+listTask[j])
						);
						//console.log(localStorage.getItem("task"+listGr[i]+"."+listTask[j])+":"+listGr[i]+":"+listTask[j]);
				}
			}
		}
	}
}

//удаляем ВСЁ
function delAll(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		for (i=0; i<listGr.length-1;i++){
			if (document.getElementById("pr-"+listGr[i])){
				document.getElementById("main").removeChild(document.getElementById("pr-"+listGr[i]));
			}
		}
	}
	changeIcon("none");
	var multi = localStorage.getItem("multi");
	localStorage.clear();
	localStorage.setItem("multi",multi);
}
//удаляем группу
function removeGr(){
	var grid = this.id.split("-")[1];
	var group = document.getElementById("pr-"+grid);
	//удалим все задачи
	if (localStorage.getItem("listTask"+grid)){
		var listTask = localStorage.getItem("listTask"+grid).split("|");
		for (var j=0; j<listTask.length-1; j++){
			//console.log("minust-"+grid+"."+listTask[j]);
			document.getElementById("minust-"+grid+"."+listTask[j]).click(); 
		}
	}
	localStorage.removeItem("listTask"+grid);
	
	document.getElementById("main").removeChild(group);
	//grtitle-# - название задачи
	//grlist-# - состояние группы (свернуто/развернуто)
	//grcolor-# - цвет задачи
	localStorage.removeItem("grtitle"+grid);
	localStorage.removeItem("grlist"+grid);
	localStorage.removeItem("grcolor"+grid);
	localStorage.removeItem("grstatus"+grid);
	
	
	//удалим id из списка групп
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var newList = "";
		for (var i=0; i<listGr.length; i++){
			if (listGr[i]!=grid&&listGr[i]!=""){
				newList += listGr[i] + "|";
			}
		}
		localStorage.setItem("listGr",newList);
	}
	//пересчитаем сумму
	sumGr();
}

//меняем идентификаторы элементов
function changeAtGr(name, grid){
	//console.log(name);
	document.getElementsByName(name+"-0")[1].id=name+"-"+grid;
	document.getElementsByName(name+"-0")[1].setAttribute("name",name+"-"+grid);
}

//меняем идентификаторы задачи
//
function changeAtTask(grid,id,srcGr,srcId){
	
	var elements = "task|link|linkin|time|playt|editt|rstt|minust|statusAt|statusBt|statusCt|statusDt|statusEt|statusFt|accept|undo|cltask|cllinkin|statusTask|editTaskPanel|dragTask|taskRow|taskRow2|";
	var listElements = elements.split("|");
	if (srcGr==null||srcId==null){//если источник не задан, то меняем нули на новую задачу
		
		for (i=0;i<listElements.length-1;i++){
			
			document.getElementsByName(listElements[i]+"-0.0")[1].id=listElements[i]+"-"+grid+"."+id;
			document.getElementsByName(listElements[i]+"-0.0")[1].setAttribute("name",listElements[i]+"-"+grid+"."+id);
		}
	}else{//если источник задан, то меняем его на новый элемент, старый не трогаем.
		for (i=0;i<listElements.length-1;i++){
			console.log("| "+grid+id+srcGr+srcId+"|"+listElements[i]);
			document.getElementById(listElements[i]+"-"+srcGr+"."+srcId).name=listElements[i]+"-"+grid+"."+id;
			document.getElementById(listElements[i]+"-"+srcGr+"."+srcId).id=listElements[i]+"-"+grid+"."+id;
		}
		
		document.getElementById(srcGr+"."+srcId).id = grid+"."+id;
		//document.getElementById(grid+"."+id).id = srcGr+"."+srcId;
		//document.getElementById("buf").id = grid+"."+id;
		/*for (i=0;i<listElements.length-1;i++){
		//changeAtTask(listElements[i],grid,id,0,0);
		//document.getElementsByName(listElements[i]+"-"+srcGr+"."+srcId)[1].id=name+"-"+grid+"."+id;
		//src to buffer
		var bufferSrc = document.getElementById(listElements[i]+"-"+srcGr+"."+srcId).id;
		//dist to Buffer
		var bufferDst = document.getElementById(listElements[i]+"-"+grid+"."+id).id;
		//src to dist
		document.getElementById(listElements[i]+"-"+srcGr+"."+srcId).id = bufferDst;
		//dist to src
		document.getElementById(listElements[i]+"-"+grid+"."+id).id = bufferSrc; 
		//set the name to id
		document.getElementById(listElements[i]+"-"+srcGr+"."+srcId).setAttribute("name",bufferSrc);
		document.getElementById(listElements[i]+"-"+grid+"."+id).setAttribute("name",bufferDst);
		//set main id
		bufferSrc = document.getElementById(srcGr+"."+srcId).id;
		bufferDst = document.getElementById(grid+"."+id).id;
		document.getElementById(srcGr+"."+srcId).id = bufferDst;
		document.getElementById(grid+"."+id).id = bufferSrc;
		}*/
		//console.log("playt-"+srcGr+"."+srcId);
		
		/*var taskSrcBuf = {name:null, linkt:null, time:null, status:null, type:null};
		taskSrcBuf.name = document.getElementById("task-"+srcGr+"."+srcId).value;
		taskSrcBuf.linkt = document.getElementById("linkin-"+srcGr+"."+srcId).value;
		taskSrcBuf.status = document.getElementById("playt-"+srcGr+"."+srcId).value;
		var typeStr = document.getElementById("statusTask-"+srcGr+"."+srcId).src.split("/");
		taskSrcBuf.type = typeStr[typeStr.length-1];
		taskSrcBuf.time = document.getElementById("time-"+srcGr+"."+srcId).placeholder;
		//если состояние рабоыт разное, меняем местами.
		//console.log(localStorage.getItem("multi"));
		if(document.getElementById("playt-"+grid+"."+id).value != taskSrcBuf.status){
			//console.log(document.getElementById("playt-"+srcGr+"."+srcId));
			//console.log("playt-"+srcGr+"."+srcId);
			//console.log(taskSrcBuf);
			
			if (localStorage.getItem("multi")=="1"){
				//console.log("Multi = 1");
				if(taskSrcBuf.status=="1"){
					//console.log("Multi = 0");
					document.getElementById("playt-"+grid+"."+id).click();
					//document.getElementById("playt-"+srcGr+"."+srcId).click();
				}else{
					document.getElementById("playt-"+srcGr+"."+srcId).click();
					//document.getElementById("playt-"+grid+"."+id).click();
				}
			}else{
				//console.log("Multi = 0");
				document.getElementById("playt-"+srcGr+"."+srcId).click();
				document.getElementById("playt-"+grid+"."+id).click();
			}
		}
		// src = dist
		document.getElementById("task-"+srcGr+"."+srcId).value = document.getElementById("task-"+grid+"."+id).value;
		document.getElementById("linkin-"+srcGr+"."+srcId).value = document.getElementById("linkin-"+grid+"."+id).value;
		document.getElementById("statusTask-"+srcGr+"."+srcId).src = document.getElementById("statusTask-"+grid+"."+id).src;
		document.getElementById("time-"+srcGr+"."+srcId).placeholder = document.getElementById("time-"+grid+"."+id).placeholder;
		document.getElementById("time-"+srcGr+"."+srcId).value = document.getElementById("time-"+grid+"."+id).placeholder;
		
		//dist = src
		document.getElementById("task-"+grid+"."+id).value = taskSrcBuf.name;
		document.getElementById("linkin-"+grid+"."+id).value = taskSrcBuf.linkt;
		document.getElementById("statusTask-"+grid+"."+id).src = taskSrcBuf.type;
		document.getElementById("time-"+grid+"."+id).placeholder = taskSrcBuf.time;
		document.getElementById("time-"+grid+"."+id).value = taskSrcBuf.time;
				
		//save the changed
		saveTaskLink(grid+"."+id);
		saveTaskName(grid+"."+id);
		saveTaskStatus(grid+"."+id);
		saveTaskTime(grid+"."+id);
		saveTaskType(grid+"."+id);
		saveTaskLink(srcGr+"."+srcId);
		saveTaskName(srcGr+"."+srcId);
		saveTaskStatus(srcGr+"."+srcId);
		saveTaskTime(srcGr+"."+srcId);
		saveTaskType(srcGr+"."+srcId);*/
		
		
		
	}
	
}

//for test
function test(){
	document.getElementById("1.1").className="lineTask dragOver";
}

//перемещает элемент перед текущим
function moveTask(grid,id,srcGr,srcId){
	//вставим новый элемент впереди
	insertBef(srcGr+"."+srcId, grid+"."+id);
	//поместим исходный элемент в буффер
	changeAtTask("b","b",srcGr,srcId);
	//изменим индесацию элементов
	var listTask = localStorage.getItem("listTask1").split("|");
	//если поднимаем выше, то все элементы после нужно увеличить
	if (srcId>id){
		console.log(listTask);
		
		for (var i=listTask.length-1; i>=0; i--){//пойдем в обратном с конца направлении, чтобы элементы не дублировали друг друга
			if(listTask[i]>=id&&listTask[i]<srcId){
				console.log("grid="+grid+"\nid="+(parseInt(listTask[i])+1)+"\nsrcGr="+grid+"\nsrcId="+listTask[i]+" i="+i);
				changeAtTask(grid,parseInt(listTask[i])+1,grid,listTask[i]);
			}
		}
		//теперь поставим наш элемент на место конечного
		changeAtTask(grid,id,"b","b");
	}else{//опускаем элементы ниже, следовательно, уменьшаем номера всех, что будут выше нашего
		for (var i=0; i<=listTask.length-1; i++){
			if(listTask[i]<id&&listTask[i]>srcId){
				console.log("grid="+grid+"\nid="+(parseInt(listTask[i])-1)+"\nsrcGr="+grid+"\nsrcId="+listTask[i]+" i="+i);
				changeAtTask(grid,parseInt(listTask[i])-1,grid,listTask[i]);
			}
		}
		//теперь поставим наш элемент на место конечного
		changeAtTask(grid,id-1,"b","b");
	}
}

//сохраним введеные значения полей, на случай если пропал фокус окна
function saveInput(){
	localStorage.setItem(this.id,this.value);
}

//сохраним в памяти состояние задач
function saveGroup(id){
	//#,#.#,#.#,#.#|#,#.#,#.#|--1,1.1,1.2,1.3|2,2.1,2.4| - список групп, задач
	//1|2|3|4|5|6
	//grtitle-# - название задачи
	//grlist-# - состояние группы (свернуто/развернуто)
	//grcolor-# - цвет задачи
	//var id = this.id.split("-")[1];
	localStorage.setItem("grtitle"+id,document.getElementById("pr_name-"+id).value);
	localStorage.setItem("grstatus"+id,document.getElementById("pr_name-"+id).disabled);
	localStorage.setItem("grcolor"+id,document.getElementById("pr-"+id).style.background);
}
//экспортировать в CSV 
function getTextgr(id){
	if (localStorage.getItem("listTask"+id)){
		var listTask = localStorage.getItem("listTask"+id).split("|");
		var textToSave = "";
		var proj = localStorage.getItem("grtitle"+id);
		for (var j=0; j<listTask.length-1; j++){
			var task = localStorage.getItem("task"+id+"."+listTask[j]);
			var time = localStorage.getItem("time"+id+"."+listTask[j]);
			textToSave += proj+";"+task+";"+time+";"+FtimeToSec(time)+";\n"
		}
		return textToSave;
	}
	return "";
}
function exportCSVgr(){
	var id = this.id.split("-")[1];
	var textToSave = "Project;Task;Time;Sec;\n";
	textToSave += getTextgr(id);
	
	var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
	var fileNameToSaveAs = "myWork.csv";
	var downloadLink = document.getElementById("exportCSVgr-"+id);
	var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
	downloadLink.download = fileNameToSaveAs;
	downloadLink.href = textToSaveAsURL;
	downloadLink.click();
	
}
function exportAll(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var textToSave = "Project;Task;Time;Sec;\n";
		for (var i=0; i<listGr.length-1; i++){
			textToSave += getTextgr(listGr[i]);
		}
		var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
		var fileNameToSaveAs = "myWork.csv";
		var downloadLink = document.getElementById("exportCSVlnk");
		var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
		downloadLink.download = fileNameToSaveAs;
		downloadLink.href = textToSaveAsURL;
		downloadLink.click();
	}
}

//добавление новой задачи
function addTask(){
	var grid = this.id.split("-")[1];
	var id = getTasId(grid);
	if (document.getElementById("nametask-"+grid).value==""){
		var task = "Some task - "+grid+"."+id;
	}else{
		var task = document.getElementById("nametask-"+grid).value;
	}
	var lnk = document.getElementById("namelink-"+grid).value.match(/^http[s]{0,1}:|^ftp[s]{0,1}:|^mailto:/g);
	var linkt = document.getElementById("namelink-"+grid).value;
	if (!lnk){
		linkt = "http://"+linkt;
		document.getElementById("namelink-"+grid).value = linkt;
	}
	//зададим тип задачи
	var typeStr = document.getElementById("statusTask-"+grid).src.split("/");
	//alert(typeStr.length);
	var type = typeStr[typeStr.length-1];
	
	createTask(
		grid,
		id,
		task,
		linkt,
		"00:00:00",
		"1",
		type);
		
		//alert(document.getElementById("gr_task-"+grid).style.display);
	if (document.getElementById("gr_task-"+grid).style.display!="block"){
		document.getElementById("cPrName-"+grid).click();
	}
	//сохраним задачу
	localStorage.setItem("task"+grid+"."+id,task);
	localStorage.setItem("link"+grid+"."+id,linkt);
	localStorage.setItem("time"+grid+"."+id,"00:00:00");
	localStorage.setItem("status"+grid+"."+id,"1");
	localStorage.setItem("type"+grid+"."+id, type);
	//обновим список групп
	if (localStorage.getItem("listTask"+grid)){
		localStorage.setItem("listTask"+grid,localStorage.getItem("listTask"+grid)+id+"|");
	}else{
		localStorage.setItem("listTask"+grid,id+"|");
	}
	//проверим мультитаск
	if (localStorage.getItem("multi")=="1"){
		stopAll(grid+"."+id);
	}
	//сбросить введение ранее значения из полей
	document.getElementById("nametask-"+grid).value = "";
	localStorage.removeItem("nametask-"+grid);
	document.getElementById("namelink-"+grid).value = "";
	localStorage.removeItem("namelink-"+grid);
	//сохраним время всех задач
	saveAllTime();
	//пересчитаем сумму
	sumGr();
	checkStatus();
}

//функция конвертации времени */
function FtimeToSec(ftime){
	return parseInt(ftime.split(":")[0])*3600+parseInt(ftime.split(":")[1])*60+parseInt(ftime.split(":")[2]);
}
function SecToFtime(sec){
	htime = Math.floor(sec/3600);
	mtime = Math.floor((sec - htime*3600)/60);
	stime = sec - htime*3600 - mtime*60;
	return ((htime<10)?"0"+htime:htime)+":"+((mtime<10)?"0"+mtime:mtime)+":"+((stime<10)?"0"+stime:stime);
}

//создание задачи
function createTask(grid, id, task, linkt, time, status, type){
	var group = document.getElementById("gr_task-"+grid);
	var src = document.getElementById("0.0");
	var cln = src.cloneNode(true);
	cln.setAttribute("id",grid+"."+id);
	//cln.style.background = "linear-gradient(300deg, #a1c4fd 0%, "+color+" 100%)";//
	cln.style.display="inline-block";
	group.appendChild(cln);
	//var elements = "task|link|linkin|time|playt|editt|rstt|minust|statusAt|statusBt|statusCt|statusDt|statusEt|statusFt|accept|undo|cltask|cllinkin|statusTask|editTaskPanel|";
	//var listElements = elements.split("|");
	//for (i=0;i<listElements.length-1;i++){
		//changeAtTask(listElements[i],grid,id,0,0);
	//}
	changeAtTask(grid,id,null,null);
	
	document.getElementById("task-"+grid+"."+id).value = task;
	document.getElementById("linkin-"+grid+"."+id).value = linkt;
	document.getElementById("time-"+grid+"."+id).value = time;
	document.getElementById("time-"+grid+"."+id).placeholder = time;
	document.getElementById("statusTask-"+grid+"."+id).src = type;
	document.getElementById("playt-"+grid+"."+id).value = status;
	if (status=="1"){
		document.getElementById("playt-"+grid+"."+id).src="pause.png";
		document.getElementById("time-"+grid+"."+id).className="field_label_tmr blink";
	}else{
		document.getElementById("playt-"+grid+"."+id).src="play.png";
		document.getElementById("time-"+grid+"."+id).className="field_label_tmr";
	}
	
	//добавим евенты
	document.getElementById("editt-"+grid+"."+id).addEventListener("click",editTask,false);
	document.getElementById("undo-"+grid+"."+id).addEventListener("click",undoTask,false);
	document.getElementById("accept-"+grid+"."+id).addEventListener("click",acceptTask,false);
	document.getElementById("rstt-"+grid+"."+id).addEventListener("click",resetTime,false);
	document.getElementById("playt-"+grid+"."+id).addEventListener("click",changePlay,false);
	document.getElementById("minust-"+grid+"."+id).addEventListener("click",removeTask,false);
	//события изменения статуса задачи
	document.getElementById("statusAt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("statusBt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("statusCt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("statusDt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("statusEt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("statusFt-"+grid+"."+id).addEventListener("click",setTypeTask,false);
	document.getElementById("link-"+grid+"."+id).addEventListener("click",golink,false);
	document.getElementById("cllinkin-"+grid+"."+id).addEventListener("dblclick",copyfield,false);
	document.getElementById("cltask-"+grid+"."+id).addEventListener("dblclick",copyfield,false);
	//евенты для драг дропа
	document.getElementById(grid+"."+id).addEventListener("dragover",allowDrop, false);
	document.getElementById(grid+"."+id).addEventListener("drop",drop,false);
	document.getElementById(grid+"."+id).addEventListener("dragenter",dragtEnter,false);
	//document.getElementById(grid+"."+id).addEventListener("dragleave",dragtLeave,false);
	//document.getElementById("dragTask-"+grid+"."+id).addEventListener("dragstart",drag, false);
}

//function highlight drag line
function dragOver(lineId){
	document.getElementById(lineId);
}
//drag and drop Function
function allowDrop(ev){
	ev.preventDefault();
	//console.log("allow drop!");
	var strId = ev.target.id.split("-");
	id = strId[strId.length-1];
	//console.log(id);
	
	
/*	if (bufId!=null&&bufId!=id){
		console.log("hurraaa1");
		document.getElementById(bufId).className="lineTask";
		bufId = id;
	}else{
		document.getElementById(id).className="lineTask dragOver";
		bufId = id;
	}*/
	
	//dragLigth(ev.target.id)
}
function drag(ev){
	//ev.dataTransfer.setData("text",ev.target.id);
	console.log("drag");
	console.log(ev.target);
}
function drop(ev){
	ev.preventDefault();
	//var data = ev.dataTransfer.getData("text");
	console.log("drop");
	console.log(ev.target);
	//console.log(data);
}
var globalDrag=null;
function dragtEnter(ev){
	console.log("dragEnter");
	ev.preventDefault();
	
	var strId = ev.target.id.split("-");
	id = strId[strId.length-1];
	console.log(id);
	console.log(globalDrag);
	if (id!=globalDrag){
		document.getElementById(id).className="lineTask dragOver";
		if(globalDrag!=null)document.getElementById(globalDrag).className="lineTask";
		globalDrag = id;
	}	
}

function dragtLeave(ev){
	//console.log("dragLeave");
	ev.preventDefault();
	var strId = ev.target.id.split("-");
	id = strId[strId.length-1];
	//console.log(id);
	//document.getElementById(id).className="lineTask";
}

//функция вставки элемента после другого
function insertAfter(newNode, referanceNode){
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

//функция вставки элемента перед другим
function insertBef(newIdNode, refIdNode){
	//my_elem.parentNode.insertBefore(span, my_elem);
	var newNode = document.getElementById(newIdNode);
	var refNode = document.getElementById(refIdNode);
	//var cln = referanceNode.cloneNode(true);
	//console.log(refNode);
	//console.log(newNode);
	refNode.parentNode.insertBefore(newNode, refNode);	
}


//копирование при двойном клике
function copyfield(){
	//var clip = document.getElementById(this.id.substr(2)).value;
	//console.log(this.id);
	document.getElementById(this.id.substr(2)).disabled=false;
	document.getElementById(this.id.substr(2)).select();
	//clip.select();
	document.execCommand("Copy");
	document.getElementById(this.id.substr(2)).disabled=true;
}

//переход на спраку
function goHelp(){
	window.open("https://youtu.be/seQiob2zvRs","_blank");
}

//переход по ссылке
function golink(){
	var id = this.id.split("-")[1];
	window.open(document.getElementById("linkin-"+id).value,"_blank");
}
function goYM(){
	window.open("https://money.yandex.ru/to/410015066499354","_blank");
}
//переход в магазин
function goStar(){
	window.open("https://chrome.google.com/webstore/detail/simple-time-track/icepjdonhcfndpmkpdeifhogkadpdojc","_blank");
}

//назначение статуса задачи
function setStatusTask(){
	var id = this.id.split("-")[1];
	document.getElementById("statusTask-"+id).src = this.src;
}
//изменение статуса задачи
function setTypeTask(){
	var idt = this.id.split("-")[1];
	document.getElementById("statusTask-"+idt).src = this.src;
}

//удалить задачу
function removeTask(){
	var id = this.id.split("-")[1];
	localStorage.removeItem("colort"+id);
	localStorage.removeItem("link"+id);
	localStorage.removeItem("status"+id);
	localStorage.removeItem("task"+id);
	localStorage.removeItem("time"+id);
	var grid = id.split(".")[0];
	var lid = id.split(".")[1];
	//console.log(id);
	var task = document.getElementById(id);
	document.getElementById("gr_task-"+grid).removeChild(task);
	//удалим id из списка групп
	if (localStorage.getItem("listTask"+grid)){
		var listTask = localStorage.getItem("listTask"+grid).split("|");
		var newList = "";
		for (var i=0; i<listTask.length; i++){
			if (listTask[i]!=lid&&listTask[i]!=""){
				newList += listTask[i] + "|";
			}
		}
		localStorage.setItem("listTask"+grid,newList);
	}
	//пересчитаем сумму
	sumGr();
	checkStatus();
}

//панель изменения задачи
function editTask(){
	var id = this.id.split("-")[1];
	document.getElementById("editTaskPanel-"+id).style.display="inline-block";
	document.getElementById("editt-"+id).removeEventListener("click",editTask,false);
	document.getElementById("editt-"+id).addEventListener("click",undoTask,false);
	document.getElementById("task-"+id).disabled=false;
	document.getElementById("task-"+id).style.background="white";
	document.getElementById("linkin-"+id).disabled=false;
	document.getElementById("linkin-"+id).style.background="white";
	document.getElementById("time-"+id).disabled=false;
	document.getElementById("time-"+id).style.background="white";
}

//закрыть панель изменения задачи
function undoTask(){
	var id = this.id.split("-")[1];
	document.getElementById("editTaskPanel-"+id).style.display="none";
	document.getElementById("editt-"+id).addEventListener("click",editTask,false);
	document.getElementById("editt-"+id).removeEventListener("click",undoTask,false);
	document.getElementById("task-"+id).disabled=true;
	document.getElementById("task-"+id).style.background="none";
	document.getElementById("task-"+id).value = localStorage.getItem("task"+id);
	document.getElementById("linkin-"+id).disabled=true;
	document.getElementById("linkin-"+id).style.background="none";
	document.getElementById("linkin-"+id).value = localStorage.getItem("link"+id);
	//alert(id);
	document.getElementById("statusTask-"+id).src = localStorage.getItem("type"+id);
	document.getElementById("time-"+id).disabled=true;
	document.getElementById("time-"+id).style.backgroundColor="#79ff03";	
}
//закрыть панель изменения задачи сохранив изменения
function acceptTask(){
	var id = this.id.split("-")[1];
	document.getElementById("editTaskPanel-"+id).style.display="none";
	document.getElementById("editt-"+id).addEventListener("click",editTask,false);
	document.getElementById("editt-"+id).removeEventListener("click",undoTask,false);
	document.getElementById("task-"+id).disabled=true;
	document.getElementById("task-"+id).style.background="none";
	localStorage.setItem("task"+id,document.getElementById("task-"+id).value);
	document.getElementById("linkin-"+id).disabled=true;
	document.getElementById("linkin-"+id).style.background="none";
	localStorage.setItem("link"+id,document.getElementById("linkin-"+id).value);
	var typeStr = document.getElementById("statusTask-"+id).src.split("/");
	localStorage.setItem("type"+id,typeStr[typeStr.length-1]);
	document.getElementById("time-"+id).disabled=true;
	document.getElementById("time-"+id).style.backgroundColor="#79ff03";
	var res = document.getElementById("time-"+id).value.match(/[0-9]{2,3}:[0-9]{2}:[0-9]{2}/g);
	if(res){ // выставляем время 
		document.getElementById("time-"+id).placeholder = SecToFtime(FtimeToSec(res[0]));
	}
	//saveAllTime();
	saveTaskName(id);
	saveTaskLink(id);
	saveTaskTime(id);
}

//сброс таймера
function resetTime(){
	var id = this.id.split("-")[1];
	document.getElementById("time-"+id).placeholder = "00:00:00";
	document.getElementById("time-"+id).value = "00:00:00";
	saveTaskTime(id);
}
//сброс всех таймеров
function resetAll(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		for (var i=0; i<listGr.length-1; i++){
			//console.log("rstgr-"+listGr[i]);
			document.getElementById("rstgr-"+listGr[i]).click();
		}
	}
	sumGr();
}

//изменить параметры мультитаска
function multi(){
	var btn = document.getElementById("multitask");
	if (localStorage.getItem("multi")){
		if (localStorage.getItem("multi")=="1"){
			localStorage.setItem("multi","0");
			btn.src = "chbox.png";
		}else{
			localStorage.setItem("multi","1");
			btn.src = "radio.png";
			stopAll("");
		}
	}else{
		localStorage.setItem("multi","1");
		btn.src = "radio.png";
		stopAll("");
	}
}

//сброс таймеров группы
function resetTimeGr(){
	var id = this.id.split("-")[1];
	if (localStorage.getItem("listTask"+id)){
		var listTask = localStorage.getItem("listTask"+id).split("|");
		for (var i=0; i<listTask.length-1; i++){
			console.log("rstt-"+id+"."+listTask[i]);
			document.getElementById("rstt-"+id+"."+listTask[i]).click();
		}
	}
}
//выключим все таймеры
function stopAll(id){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		for (var i=0; i<listGr.length-1;i++){
			if (localStorage.getItem("listTask"+listGr[i])){
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				for (var j=0; j<listTask.length-1; j++){
					if (document.getElementById("playt-"+listGr[i]+"."+listTask[j]).value=="1"){
						if (id!=listGr[i]+"."+listTask[j]){
							document.getElementById("playt-"+listGr[i]+"."+listTask[j]).click();
						}
					}
				}
			}
		}
		if (id==""){
			changeIcon("stop");
		}
	}
}
//меняем иконку в зависимости от состояния */
function changeIcon(status){
	switch(status){
		case "run":
			chrome.browserAction.setIcon({path:"49run.png"});
			//console.log("run");
			break;
		case "stop":
			chrome.browserAction.setIcon({path:"49stop.png"});
			break;
		case "none":
			chrome.browserAction.setIcon({path:"49.png"});
			break;
	}
}
//меняем местами идентификаторы проектов
function swapGroup(src, dist){
	
}

//меняем местами идентификаторы задач
function swapTask(src,dist){
	
}

// проверяет статус, если есть хотя бы 1 задача во вкл, вернет run, если задач нет вернет nope
function checkStatus(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var status = false;
		var hastask = false;
		for (var i=0; i<listGr.length-1;i++){
			if (localStorage.getItem("listTask"+listGr[i])){
				var statusgr = false;
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				for (var j=0; j<listTask.length-1; j++){
					hastask = true;
					if (document.getElementById("playt-"+listGr[i]+"."+listTask[j]).value=="1"){
						statusgr = true;
						status = true;
						break;
					}
				}
				if (statusgr){
					document.getElementById("sumtaskgr-"+listGr[i]).className="sumtsk blink";
				}else{
					document.getElementById("sumtaskgr-"+listGr[i]).className="sumtsk";
				}
			}
		}
		if(hastask){
			if (status){
				changeIcon("run");
				//console.log("start");
			}else{
				changeIcon("stop");
				//console.log("stop");
			}
		}else{
			changeIcon("none");
			//console.log("none");
		}
		//console.log("chstatus: "+status+"; hastask: "+hastask);
		return 1;
	}else{
		changeIcon("none");
		//console.log("none2");
	}
}

//переключение таймера задач
function playTimer(id){
	document.getElementById("playt-"+id).value="1";
	document.getElementById("playt-"+id).src="pause.png"
	document.getElementById("time-"+id).className="field_label_tmr blink";
	if (localStorage.getItem("multi")=="1"){
		stopAll(id);
	}
	changeIcon("run");
	saveTaskStatus(id);
}
function stopTimer(id){
	document.getElementById("playt-"+id).value="0";
	document.getElementById("playt-"+id).src="play.png"
	document.getElementById("time-"+id).className="field_label_tmr";
	saveTaskStatus(id);
	saveTaskTime(id);
	//console.log(code);
}
function changePlay(){
	var id = this.id.split("-")[1];
	if (document.getElementById("playt-"+id).value=="1"){
		stopTimer(id);
	}else{
		playTimer(id);
	}
	checkStatus();
}

//функция отсчета таймера */
function updateTime(id){
	var time = document.getElementById("time-"+id).placeholder;
	var ftime = SecToFtime(parseInt(FtimeToSec(time))+1);
	document.getElementById("time-"+id).placeholder = ftime;
	if (document.getElementById("task-"+id).style.background=="none"){
		document.getElementById("time-"+id).value = ftime;
	}
}

//пробежим по всем включенным таймерам и отсчетаем время
function timer(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		for (var i=0; i<listGr.length; i++){
			if (localStorage.getItem("listTask"+listGr[i])){
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				for (var j=0; j<listTask.length-1;j++){
					//console.log("playt-"+listGr[i]+"."+listTask[j]);
					if (document.getElementById("playt-"+listGr[i]+"."+listTask[j]).value=="1"){
						updateTime(listGr[i]+"."+listTask[j]);
					}
				}
			}
		}
	}
}

//вернуть номер задачи/группы в зависимости от проекта
function getTasId(grid){
	if (localStorage.getItem("listTask"+grid)){
		var listTask = localStorage.getItem("listTask"+grid).split("|");
		var taskid = parseInt(listTask[listTask.length-2])+1;
		return taskid;
	}else{
		return "1";
	}
}
function getGrId(){
	//выявим номер группы [0.1,0.2|1.1,1.2]
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var grid = parseInt(listGr[listGr.length-2])+1;
		//console.log("grid="+grid);
	}else{
		var grid = '1';
	}
	return grid;
}

//сохранить все параметры и штамп времени
function saveAllTime(){

	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var date = new Date();
		for (var i=0; i<listGr.length; i++){
			if (localStorage.getItem("listTask"+listGr[i])){
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				for (var j=0; j<listTask.length-1;j++){
					//console.log("playt-"+listGr[i]+"."+listTask[j]);
					if (document.getElementById("playt-"+listGr[i]+"."+listTask[j]).value=="1"){
						//updateTime(listGr[i]+"."+listTask[j]);
						saveTaskTime(listGr[i]+"."+listTask[j]);
						localStorage.setItem("timestamp",Math.round(date.getTime()/1000));
					}
				}
			}
		}
	}
}
// save task type
function saveTaskType(id){
	var typeStr = document.getElementById("statusTask-"+id).src.split("/");
	localStorage.setItem("type"+id,typeStr[typeStr.length-1]);
}
//сохранить состояние задачи
function saveTaskTime(id){
	localStorage.setItem("time"+id,document.getElementById("time-"+id).placeholder);
}
function saveTaskName(id){
	localStorage.setItem("task"+id,document.getElementById("task-"+id).value);
}
function saveTaskLink(id){
	localStorage.setItem("link"+id,document.getElementById("linkin-"+id).value);
}
function saveTaskStatus(id){
	localStorage.setItem("status"+id,document.getElementById("playt-"+id).value);
}
/*function saveTaskType(id){
	localStorage.setItem("tskType"+id,document.getElementById("statusTask-"+grid+"."+ig).src);
}*/

//сумма всех таймеров
function sumGr(){
	if (localStorage.getItem("listGr")){
		var listGr = localStorage.getItem("listGr").split("|");
		var sumTotal = 0;
		for (var i=0; i<listGr.length; i++){
			if (localStorage.getItem("listTask"+listGr[i])){
				var listTask = localStorage.getItem("listTask"+listGr[i]).split("|");
				var summ=0;
				for (var j=0; j<listTask.length-1; j++){
					summ += parseInt(FtimeToSec(document.getElementById("time-"+listGr[i]+"."+listTask[j]).placeholder));
					
				}
				sumTotal += summ;
				document.getElementById("sumtgr-"+listGr[i]).value=SecToFtime(summ);
				document.getElementById("sumtaskgr-"+listGr[i]).value=listTask.length-1;
				//console.log("gr-"+listGr[i]+" summ: "+summ+": "+SecToFtime(summ));
			}
		}
		document.getElementById("sumall").value=SecToFtime(sumTotal);
		document.getElementById("sumtaskgr").value=listGr.length-1;
	}
}

//

//LISTENERS and STORAGE------------------------------------------------------
window.onload=function(){
	//восстановим группы
	restoreGr();
	//восстановим главную панель
	if (localStorage.getItem("multi")=="1"){
		document.getElementById("multitask").src="radio.png";
	}else{
		document.getElementById("multitask").src="chbox.png";
	}
	//восстановим значение которое вводили ранее в поле
	if (localStorage.getItem("proj")){
		document.getElementById("proj").value = localStorage.getItem("proj");
	}
	//эвент для теста
	document.getElementById("test").addEventListener("click",test,false);
	document.getElementById("show-add").addEventListener("click",showGrIn,false);//кнопка раскрытия ввода группы
	//выбор цвета при вводе группы
	document.getElementById("colorA").addEventListener("click",setColorGr,false);
	document.getElementById("colorB").addEventListener("click",setColorGr,false);
	document.getElementById("colorC").addEventListener("click",setColorGr,false);
	document.getElementById("colorD").addEventListener("click",setColorGr,false);
	document.getElementById("colorE").addEventListener("click",setColorGr,false);
	document.getElementById("colorF").addEventListener("click",setColorGr,false);
	//добавление новой группы
	document.getElementById("add_group").addEventListener("click",addGr,false);
	//показать панель добавления задачи
	//document.getElementById("show_task-0").addEventListener("click",showGrTask,false);
	//показать панель добавления задачи
	document.getElementById("show_add_panel-0").addEventListener("click",showGrTaskPanel,false);
	document.getElementById("editgr-0").addEventListener("click",showGrEdit,false);
	document.getElementById("undogr-0").addEventListener("click",undoGrEdit,false);
	document.getElementById("acceptgr-0").addEventListener("click",acceptGrEdit,false);
	//выбор цвета текущей группы
	/*document.getElementById("colorAgr-0").addEventListener("click",setColorCurGr,false);
	document.getElementById("colorBgr-0").addEventListener("click",setColorCurGr,false);
	document.getElementById("colorCgr-0").addEventListener("click",setColorCurGr,false);
	document.getElementById("colorDgr-0").addEventListener("click",setColorCurGr,false);
	document.getElementById("colorEgr-0").addEventListener("click",setColorCurGr,false);
	document.getElementById("colorFgr-0").addEventListener("click",setColorCurGr,false);*/
	//добавление задачи
	document.getElementById("addtask-0").addEventListener("click",addTask,false);
	//удалить ВСЁ
	document.getElementById("drop").addEventListener("click",delAll,false);
	//сбросить всё
	document.getElementById("reset").addEventListener("click",resetAll,false);
	//кнопка мульти
	document.getElementById("multitask").addEventListener("click",multi,false);
	//экспорт всех задач
	document.getElementById("exportCSV").addEventListener("click",exportAll,false);
	//Сохраним значение поля группы на случай сворачивания окна
	document.getElementById("proj").addEventListener("keyup",saveInput,false);
	//Переход на ям
	document.getElementById("ym").addEventListener("click",goYM,false);
	//переход на справку
	document.getElementById("qst").addEventListener("click",goHelp,false);
	//переход в магазин
	document.getElementById("star").addEventListener("click",goStar,false);
	//checkBtn();
	sumGr();
	checkStatus();
	
	//drag and drop(
	//document.addEventListener("dragstart",drag(event));
	
}

//запустим таймер
window.setInterval(timer,1000);

//таймер отсчета суммы задач
window.setInterval(sumGr,5000);

//при сворачивании окна сохранить состояние задач
window.onblur = function(){
	saveAllTime();
}

