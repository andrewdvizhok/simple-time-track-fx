var id = 0;
var g_setting=[];


//*===============================================
//=================================================
//функция для тестов */
function test(){
	//chrome.browserAction.setIcon({path:"48.png"});
	//window.open("https://ya.ru","_blank");
}
//======================================================
//*функция для тестов */
function copyClip(){
	//chrome.browserAction.setIcon({path:"48.png"});
	//window.open("https://ya.ru","_blank");
	console.log(this.id);
	var id = this.id.split("-")[1];
	var task = this.id.split("-")[0];
	switch(task){
		case "c_task":
			var src = document.getElementById("protask-"+id);
			break;
		case "c_link":
			var src = document.getElementById("linkin-"+id);
			break;
		case "c_time":
			var src = document.getElementById("time-"+id);
			break;
		case "c_proj":
			var src = document.getElementById("proj-"+id);
			break;
	}
	if (src.disabled){
		src.disabled=false;
		src.select();
		src.disabled=true;
	}	
	src.select();
	document.execCommand("Copy");
}
//======================================================
/*функция для настроек */
function goOpt(){
	window.open("options.html?app","_self");
}

//======================================================
//*функция для перехода по ссылке */
function goLink(){
	//chrome.browserAction.setIcon({path:"48.png"});
	var id = this.id.split("-")[1];
	var lnk = document.getElementById("linkin-"+id).value;
	window.open(lnk,"_blank");
	//window.open("https://ya.ru","_blank");
	//console.log(lnk);
}
//======================================================
//*===============================================
//===============================================================================================================
//функция запуска/остановки таймера */
function control(){
	var len = this.src.split("/").length;
	//console.log(this.src.split("/"));
	//console.log(len);
	var btn = this.src.split("/")[len-1];
	var id = this.id.split("-")[1];
	switch (btn){
		case "play.png": 
			this.src="pause.png";
			this.value="1";
			changeIcon("run");
			localStorage.setItem("ctrl_btn-"+id,"1");
			
			break;
		case "pause.png":
			this.src="play.png";
			this.value="0";
			localStorage.setItem("ctrl_btn-"+id,"0");
			localStorage.setItem("time-"+id,document.getElementById("time-"+id).placeholder);
			changeIcon(checkStatus());
			break;
		case "accept.png":
			//var id = this.id.split("-")[1];
			var timer = document.getElementById("time-"+id);
			var res = timer.value.match(/[0-9]{2,3}:[0-9]{2}:[0-9]{2}/g);
			//document.getElementById("debug").innerHTML=res;
			if(res){ // выставляем время 
				timer.placeholder = SecToFtime(FtimeToSec(res[0]));
				//document.getElementById("debug").innerHTML=res;
				timer.style.color="green";
			}else{
				timer.style.color="red";
			}
			//break;
			//правим ссылку
			var links = document.getElementById("linkin-"+id);
			var lnkbtn = document.getElementById("link-"+id);
			//var nlink = document.getElementById("link-"+id);
			if (links.value != ""){
				var lnk = links.value.match(/^http[s]{0,1}:|^ftp[s]{0,1}:|^mailto:/g);
				lnkbtn.style.visibility = "visible";
				if (lnk){
					links.plcaeholder = links.value;
					//nlink.href = links;
					//nlink.innerHTML = links.substr(0,16);
				}else{
					var temp = links.value;
					links.value = "http://"+temp;
					links.plcaeholder = links.value;
					//nlink.href = "http://"+links;
					//nlink.innerHTML = "http://"+links.substr(0,16);
				}
			}else{
				links.placeholder = "";
				lnkbtn.style.visibility = "hidden";
			}
				
			saveTask(id); 
			//сохраним изменения в хранилище
			undoTask(id); 
			// внесли изменения, переведем иконки в прежний режим
			if(document.getElementById("chart").value=="block") chartBox();
			//обновим диаграммы
			break;
		
		//	undoTask(id);
		//	break;
	}
	
}
//================================================

//*===============================================
//===============================================================================================================
//меняем иконку в зависимости от состояния */
function changeIcon(status){
	switch(status){
		case "run":
			chrome.browserAction.setIcon({path:"49run.png"});
			//console.log("run");
			break;
		case "stop":
			chrome.browserAction.setIcon({path:"49stop.png"});
			break;
		case "nope":
			chrome.browserAction.setIcon({path:"49.png"});
			break;
	}
}

// проверяет статус, если есть хотя бы 1 задача во вкл, вернет run, если задач нет вернет nope
function checkStatus(){
	//console.log("test");
	if (document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		
		for (var i=0; i<=list.length-2; i++){
			if(document.getElementById("ctrl_btn-"+list[i]).value=="1"){
				//console.log("test");
				return "run";
			}
		}
		return "stop";
	}else{
		return "nope";
	}
}

//===============================================

//*===============================================
//=================================================
//функция конвертации времени */
function FtimeToSec(ftime){
	return parseInt(ftime.split(":")[0])*3600+parseInt(ftime.split(":")[1])*60+parseInt(ftime.split(":")[2]);
}
function SecToFtime(sec){
	htime = Math.floor(sec/3600);
	mtime = Math.floor((sec - htime*3600)/60);
	stime = sec - htime*3600 - mtime*60;
	return ((htime<10)?"0"+htime:htime)+":"+((mtime<10)?"0"+mtime:mtime)+":"+((stime<10)?"0"+stime:stime);
}
function addTime(ftime,diff){
	return SecToFtime(parseInt(FtimeToSec(ftime)) + parseInt(diff));
}
//========================

//*===============================================
//============================================================================================================
//функция отсчета таймера */
function updateTime(){
	//var count = parseInt(document.getElementById("id").value); // сколько всего таймеров
	var timer;
	var time;
	var ftime,htime;
	var i=0;
	
	if (document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		//console.log(document.getElementById("id").value);
		//list.splice(-1,1);
		//document.getElementById("id").value = list;//затолкаем данные в список, его использует getsetId()
		for(var i=0;i<list.length-1; i++){
			if (document.getElementById("ctrl_btn-"+list[i]).value == "1"){
				timer = document.getElementById("time-"+list[i]);
				//console.log(timer)
				time = parseInt(FtimeToSec(timer.placeholder));
				time += 1;
				timer.placeholder = SecToFtime(time);
				if (timer.style.backgroundColor != "white") timer.value=timer.placeholder;
			}
		}
	}
}
//======================================

//*===============================================
//==============================================================================================================
//функция добавления задачи */
function addTask(){
	//var body = document.getElementById("main");
	var src = document.getElementById("0");
	var cln = src.cloneNode(true);
	
	
	/* меняем название задачи */
	//=================================
	var task = document.getElementById("task");
	var links = document.getElementById("link");
	var proj = document.getElementById("proj");
	var id = getsetId();//вставим задачу и получим ее номер
	//console.log(id);
	if (task.value == ""){
		restTask(id,"Some task..."+id,"00:00:00","1",links.value,proj.value);
		//console.log(links.value);
	}else{
		restTask(id,task.value,"00:00:00","1",links.value,proj.value);
	}
	//увеличиваем счетчик
	//id++;
	//сохраним задачу
	saveTask(id);
	changeIcon("run");
	//document.getElementById("id").value = id;
	task.value = "";
	links.value = "";
	proj.value = "";
	
	//нарисуем диаграммы если нужно
	if(document.getElementById("chart").value=="block") chartBox();
	//test();
}
//==================================
/*===============================================
============================================================================================================
функция контроля строки массива задач */
function getsetId(){ //найдем место в строке и получим номер задачи
	var id = 1;
	//версия v1.1
	if (document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");//.splice(-1,1);
		//console.log("A");
		//console.log(document.getElementById("id").value);
		//console.log(list.splice(-1,1));
		//console.log(list.length-1);
		//var pos = parseInt(list.length-2);
		//console.log(pos);
		//console.log(list[pos]);
		id = (parseInt(list[list.length-2])+1);//Номер элемента больше последнего		
		var rslt = id+"|";
		document.getElementById("id").value += rslt;//добавим элемент в конец строки
	}else{
		document.getElementById("id").value = "1|";//добавим элемент 
	}
	
	//console.log(id);
	
	return id;
}


/*===============================================
============================================================================================================
функция восстановления задачи */
function restTask(id,protask,time,status,links,project){
	var src = document.getElementById("0");
	var cln = src.cloneNode(true);
	cln.setAttribute("id",id); // задаем id задачи
	//вставляем новый элемент ----------------------------------
	document.getElementById("main").appendChild(cln);
	document.getElementById(id).style.display="block";//делаем его видимым
	
	//получаем только что созданный элемент
	var ncln = document.getElementsByName("protask-0")[1];
	/* меняем название задачи */
	//=================================
	ncln.value = protask;
	//меняем имя
	ncln.name="protask-"+id;
	//меняем ид
	ncln.id="protask-"+id;
	
	/*меняем название кнопок копирования, добавляем листенер кнопок для задачи, ссылки, таймера*/
	//====================================
	var ctask = document.getElementsByName("c_task-0")[1];
	ctask.id = "c_task-"+id;
	ctask.setAttribute("name","c_task-"+id);
	ctask.addEventListener("dblclick",copyClip);
	
	//  вставис ссылку и поле ссылки если есть аднные
	/*if (links != ""){ // 
	console.log(links);*/
	//добавляем клик на линк для правки
	var clink = document.getElementsByName("c_link-0")[1];
	clink.id = "c_link-"+id;
	clink.setAttribute("name","c_link-"+id);
	/*меняем ид link */
	//================
	var nlink = document.getElementsByName("link-0")[1];
	nlink.id="link-"+id;
	nlink.name="link-"+id;
	/*меняем ид linkin */
	//================
	var nlinkin = document.getElementsByName("linkin-0")[1];
	nlinkin.id="linkin-"+id;
	nlinkin.name="linkin-"+id;
	if(document.getElementById("link").style.display=="none"){
		nlinkin.style.display="none";
		clink.style.display="none";
		nlink.style.display="none";
	}else{
		clink.addEventListener("dblclick",copyClip);
		nlink.addEventListener("click",goLink);
		//проверим ссылку, если не указали протокол, то это http
		if (links != ""){
			//ссылка есть делаем видимым кнопку
			nlink.style.visibility = "visible";
			var lnk = (links)?links.match(/^http[s]{0,1}:|^ftp[s]{0,1}:|^mailto:/g):"";
			if (lnk){
				nlinkin.value = links;
				nlinkin.placeholder = links;
				//nlink.href = links;
				//nlink.innerHTML = links.substr(0,16);
			}else{
				nlinkin.value = "http://"+links;
				nlinkin.placeholder = "http://"+links;
				//nlink.href = "http://"+links;
				//nlink.innerHTML = "http://"+links.substr(0,16);
			}
		}else{
			nlink.style.visibility = "hidden";
		}
	}
	
	
	
	
	//для копирования времени
	var ctime = document.getElementsByName("c_time-0")[1];
	ctime.id = "c_time-"+id;
	ctime.setAttribute("name","c_time-"+id);
	ctime.addEventListener("dblclick",copyClip);
	
	/*меняем название кнопки, добавляем листенер кнопки*/
	//====================================
	var nbtn = document.getElementsByName("ctrl_btn-0")[1];
	nbtn.id="ctrl_btn-"+id;
	nbtn.name="ctrl_btn-"+id;
	nbtn.addEventListener("click",control);
	nbtn.value=status; // таймер включен 
	nbtn.src=(status=="1"?"pause.png":"play.png");// ставим иконку включенного таймера
	/* переименоваываем и добавляем таймер*/
	//======================================
	var ntime = document.getElementsByName("time-0")[1];
	ntime.id="time-"+id;	
	ntime.name="time-"+id;
	ntime.value=time;
	ntime.placeholder=time;
	//ntime.addEventListener("click",copyClip);
	//ntime.addEventListener("focusout",changeTimeEnd);
	/*меняем название кнопки удаления*/
	//=======================
	var nminus = document.getElementsByName("minus-0")[1];
	nminus.id="minus-"+id;
	nminus.name="minus-"+id;
	nminus.addEventListener("click",delTask);
	
	/*меняем ид edit */
	//================
	var nedit = document.getElementsByName("edit-0")[1];
	nedit.id="edit-"+id;
	nedit.name="edit-"+id;
	nedit.addEventListener("click",editTask);
	
	/*меняем ид reset */
	//================
	var nreset = document.getElementsByName("reset-0")[1];
	nreset.id="reset-"+id;
	nreset.name="reset-"+id;
	nreset.addEventListener("click",resetTime);
	/*меняем ид proj */
	//================
	var nproj = document.getElementsByName("proj-0")[1];
	nproj.id="proj-"+id;
	nproj.name="proj-"+id;
	nproj.value = project;
	//меняем ид переноса роекта
	var nprojbr = document.getElementsByName("projbr-0")[1];
	nprojbr.id = "nprojbr-"+id;
	nprojbr.setAttribute("name","nprojbr-"+id);
	/*меняем название кнопок копирования, добавляем листенер кнопок для проекта*/
	//====================================
	var cproj = document.getElementsByName("c_proj-0")[1];
	cproj.id = "c_proj-"+id;
	cproj.setAttribute("name","c_proj-"+id);
	//проверяем настройки, отображать ли нам proj
	if (document.getElementById("proj").style.display=="none" ){
		cproj.style.display="none";
		nproj.style.display="none";
		nprojbr.style.display="none";
	}else{
		nproj.style.display="block";
		nprojbr.style.display="block";
		cproj.addEventListener("dblclick",copyClip);
	}
	
	//нарисуем диаграммы если требуется
	var nchart = document.getElementsByName("chart-0")[1];
	nchart.id = "chart-"+id;
	nchart.setAttribute("name","chart-"+id);
	var nchartb = document.getElementsByName("chartb-0")[1];
	nchartb.id = "chartb-"+id;
	nchartb.setAttribute("name","chartb-"+id);
	nchart.style.display = document.getElementById("chart").value;
	nchartb.style.display = document.getElementById("chart").value;
	
}
//===============================================

/*===============================================
=================================================
функция удаления задачи или отмена действия редактирования */
function delTask(){
	var id = this.id.split("-")[1];
	//проверим действие, если отмена правки, то удалять не надо
	//if(document.getElementById("protask-"+id).disabled){
		var task = document.getElementById(id);
		//var timer = document.getElementById("time-"+id);
		//var ctrl = document.getElementById("ctrl_btn-"+id);
		var main = document.getElementById("main");
		main.removeChild(task);
			//console.log("nop");
		//}
		//проверим остались ли ещё элементы, если удалили последний, то очистим память
		var list = document.getElementById("id").value.replace(id+"|","");
		document.getElementById("id").value = list;
		if (list==""){//удалили все элементы очищаем
			myf_drop();
			
		}else{//не все элементы, удаляем текущий
			removeTask(id);
			changeIcon(checkStatus());
			// обновим диаграммы для остальных
			if(document.getElementById("chart").value=="block") chartBox(); 
		}	
}
//================================
/*===============================================
=================================================
функция изменения задачи*/
function myf_drop(){
	//var count = parseInt(document.getElementById("id").value);
	if(document.getElementById("id").value!=""){
		var list = document.getElementById("id").value.split("|");
		var main = document.getElementById("main");
		//var task;
		
		
		for(var i=0;i<list.length-1; i++){
			//removeTask(list[i]);
			if (document.getElementById(list[i])){
				var task = document.getElementById(list[i]);
				//console.log(task);
				main.removeChild(task);
			}
		}
	}
	document.getElementById("id").value=""; //обнулим счетчик
	var setting = localStorage.getItem("setting");
	localStorage.clear();
	localStorage.setItem("setting",setting);
	changeIcon("nope");
}
//================================
/*===============================================
=================================================
функция закрытия/открытия инструкции */
function clsNote(){
	if (document.getElementById("notes").style.display == "block"){
		document.getElementById("notes").style.display = "none";
	}else{
		document.getElementById("notes").style.display = "block";
	}
	//document.cookie = "test=test;t2=t2";
}
/*===============================================
=================================================
функция работы с хранилищем */

function saveTask(id){ //сохраним задачу
	//var lnk = (document.getElementById("linkin-"+id).value)?document.getElementById("linkin-"+id).value:"";
	//var proj = document.getElementById("proj-"+id).value;
	localStorage.setItem("protask-"+id,document.getElementById("protask-"+id).value);
	localStorage.setItem("time-"+id,document.getElementById("time-"+id).placeholder);
	localStorage.setItem("ctrl_btn-"+id,document.getElementById("ctrl_btn-"+id).value);
	if (document.getElementById("linkin-"+id)) {
		localStorage.setItem("link-"+id,document.getElementById("linkin-"+id).value)
	}else{localStorage.removeItem("link-"+id)};
	if (document.getElementById("proj-"+id)) {
		localStorage.setItem("proj-"+id,document.getElementById("proj-"+id).value)
	}else{localStorage.removeItem("proj-"+id)};
	//console.log(lnk);
	localStorage.setItem("list",document.getElementById("id").value);

}
function removeTask(id){//удалим задачу
	localStorage.removeItem("protask-"+id);
	localStorage.removeItem("time-"+id);
	localStorage.removeItem("ctrl_btn-"+id);
	localStorage.removeItem("link-"+id);
	localStorage.removeItem("proj-"+id);
	localStorage.setItem("list",document.getElementById("id").value);
}

function getTask(storage,page,dt){//загрузим задачу
	var time = localStorage.getItem("time-"+storage);
	var task = localStorage.getItem("protask-"+storage);
	var status = localStorage.getItem("ctrl_btn-"+storage);
	var lnk = (localStorage.getItem("link-"+storage))?localStorage.getItem("link-"+storage):"";
	var proj = (localStorage.getItem("proj-"+storage))?localStorage.getItem("proj-"+storage):"";
	if(status=="1"){//таймер был включен добавляем разницу во времени
	//document.getElementById("task").value=addTime(localStorage.getItem("time-"+storage),dt);
	//console.log(dt);
		restTask(page,task,addTime(time,dt),status,lnk,proj);
		//document.getElementsByName("id").value = "getTask";
	}else{//иначе переносим как есть
		restTask(page,task,time,status,lnk,proj);
	}
	
	
}

/*===============================================
=================================================
функция изменения задачи*/
function editTask(){
	// узнаем ид задачи
	var id = this.id.split("-")[1];
	var task = document.getElementById("protask-"+id);
	var lnk = document.getElementById("linkin-"+id);
	var proj = document.getElementById("proj-"+id);
	if (this.value == "edit"){
		//console.log(id);
		//Переключим для всех полей режим ввода, соотв. ид
		
		task.disabled = false;
		task.style.background = "white";
		task.placeholder = task.value;
		//var lnk = document.getElementById("link-"+id);
		
		//меняем для ссылки
		lnk.disabled = false;
		lnk.style.background = "white";
		lnk.placeholder = lnk.value;//забэкапим значение ссылки
		//меняем для проекта
		proj.disabled = false;
		proj.style.background = "white";
		proj.placeholder = proj.value;//бэкапим для проетка
		
		var timer = document.getElementById("time-"+id);
		timer.disabled = false;
		timer.style.background = "white";
		//меняем кнопки
		this.src = "undo.png";
		this.value = "undo";
		document.getElementById("ctrl_btn-"+id).src = "accept.png";
	}else{ // отменили изменения
		this.value = "edit";
		this.src = "edit.png"
		lnk.value = lnk.placeholder; //восстановим значение ссылки
		task.value = task.placeholder;
		proj.value = proj.placeholder;
		undoTask(id);
	}
	
	
}
function undoTask(id){
	//Переключим для всех полей режим ввода, соотв. ид
	var task = document.getElementById("protask-"+id);
	var lnk = document.getElementById("linkin-"+id);
	var proj = document.getElementById("proj-"+id);
	task.disabled = true;
	task.style.background = "none";
	proj.disabled = true;
	proj.style.background = "none";
	
	//document.getElementById("linkin-"+id).style.display = "none";
	//document.getElementById("link-"+id).style.display = "block";
	lnk.disabled = true;
	lnk.style.background = "none";
	var timer = document.getElementById("time-"+id);
	timer.disabled = true;
	timer.style.background = "none";
	//меняем кнопки
	document.getElementById("edit-"+id).src = "edit.png";
	document.getElementById("edit-"+id).value = "edit";
	document.getElementById("ctrl_btn-"+id).src = (document.getElementById("ctrl_btn-"+id).value == "0")?"play.png":"pause.png";
}

function resetTime(){
	var id = this.id.split("-")[1];
	var timer = document.getElementById("time-"+id);
	timer.placeholder="00:00:00";
	timer.value="00:00:00";
	localStorage.setItem("time-"+id,"00:00:00");
	if(document.getElementById("chart").value=="block") chartBox();//обновим диаграммы
}
/*===============================================
=================================================
Рисуем диаграммы*/
function chartBox(){
	var list = localStorage.getItem("list").split("|");//пробежимся по всем таймерам
	var maxTime = 0;
	
	var curTime = 0;
	var rc=0; 
	var gc=0;
	var widthBox = 10;
	
	var minTime = FtimeToSec(document.getElementById("time-"+list[0]).placeholder);
	for(var i=0;i<list.length-1; i++){//ищем min max массива
		curTime = FtimeToSec(document.getElementById("time-"+list[i]).placeholder);	
		if (curTime>maxTime){maxTime = curTime;}
		if (curTime<minTime){minTime = curTime;}
	}
	var deltaTime = maxTime - minTime;
	for(var i=0;i<list.length-1; i++){
		curTime = FtimeToSec(document.getElementById("time-"+list[i]).placeholder);	
		if(maxTime>90){
			gc = Math.round(255-((curTime-minTime)/deltaTime)*255);
			rc = Math.round(((curTime-minTime)/deltaTime)*255);
			widthBox = Math.round(5+((curTime-minTime)/deltaTime)*90);
			document.getElementById("chartb-"+list[i]).style.backgroundColor="rgb("+rc+","+gc+",0)";
			document.getElementById("chartb-"+list[i]).style.width=widthBox+"%";
			//document.getElementById("chartb-"+list[i]).style.background="green;";
			//document.getElementById("chartb-"+list[i]).style.backgroundColor="white";
			//console.log("rgb="+rc+","+gc);
		}else{
			document.getElementById("chartb-"+list[i]).style.backgroundColor="green";
			document.getElementById("chartb-"+list[i]).style.width="5%";
			//console.log("green");
		}
	}
	
}
//=================================================
//функция для направления на донаты
function myf_goYM(){
	//window.open("https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9D%D0%B0%20%D0%B5%D0%B4%D1%83%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC%20%2F%20For%20food%20of%20dev&targets-hint=&default-sum=10&button-text=14&payment-type-choice=on&comment=on&hint=&successURL=&quickpay=shop&account=410015066499354","_blank");
	window.open("https://money.yandex.ru/to/410015066499354","_blank");
}
//направляем на оценку приложения
function myf_goSTAR(){
	//window.open("https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9D%D0%B0%20%D0%B5%D0%B4%D1%83%20%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0%D0%BC%20%2F%20For%20food%20of%20dev&targets-hint=&default-sum=10&button-text=14&payment-type-choice=on&comment=on&hint=&successURL=&quickpay=shop&account=410015066499354","_blank");
	window.open("https://addons.mozilla.org/en-US/firefox/addon/simple-time-track-v2","_blank");
}

//================================================

//LISTENERS and STORAGE------------------------------------------------------
window.onload=function(){
	//прочетм куки для применения настроек
	//var coockie = document.cookie;
	if (localStorage.getItem("setting")){
		var setting = localStorage.getItem("setting").split(";");
		for(var i=0;i<setting.length;i++){
			switch(setting[i]){
				case "proj":
					document.getElementById("proj").style.display="block";
					break;
				case "nolink":
					document.getElementById("link").style.display="none";
					break;
				case "nodrop":
					document.getElementById("drop").style.display="none";
					break;
				case "nohelp":
					document.getElementById("qst").style.display="none";
					break;	
				case "chart":
					document.getElementById("chart").value="block";
					break;	
			}
		}
	}
	
	document.getElementById("add").addEventListener("click",addTask);
	document.getElementById("drop").addEventListener("click",myf_drop);
	document.getElementById("cls_note").addEventListener("click",clsNote);
	document.getElementById("qst").addEventListener("click",clsNote);
	document.getElementById("setting").addEventListener("click",goOpt);
	document.getElementById("star").addEventListener("click",myf_goSTAR);
	document.getElementById("ym").addEventListener("click",myf_goYM);
	document.getElementById("task").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});
    document.getElementById("link").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});
    document.getElementById("proj").addEventListener("keyup",function(event) {
    	event.preventDefault();
    		if (event.keyCode == 13) {
        		document.getElementById("add").click();
        	}
    	});
	// прочитаем данные из хранилища для восстановления таймера
	if (typeof(Storage) !== "undefined") {
		var timestamp = new Date();//посмотрим разницу
		if (localStorage.getItem("timestamp")){
			var difftime = Math.round(timestamp.getTime()/1000)-parseInt(localStorage.getItem("timestamp"));
		}else{
			var difftime = 0;
		}
	// новый тип хранилища v1.1*/
    	var realcnt = 1;
		//document.getElementById("task").value="v1.1";
		if(localStorage.getItem("list")){ //данные есть
			//document.getElementById("id").value = localStorage.getItem("list");
			//console.log(document.getElementById("id").value);
			document.getElementById("id").value = localStorage.getItem("list");
			var list = localStorage.getItem("list").split("|");
			//list.splice(-1,1);
			//document.getElementById("id").value = list;//затолкаем данные в список, его использует getsetId()
			for(var i=0;i<list.length-1; i++){
				getTask(list[i],list[i],difftime);
				
			}
		}
    }
    changeIcon(checkStatus());
    if(document.getElementById("chart").value=="block") chartBox();
}

window.onblur=function(){
	/// сохраним таймер в хранилище
	if (document.getElementById("id").value!=""){
		var date = new Date();
		localStorage.setItem("timestamp",Math.round(date.getTime()/1000));
		localStorage.setItem("list",document.getElementById("id").value);
		var list = document.getElementById("id").value.split("|");
		for(var i=0;i<list.length-1; i++){
			if (document.getElementById("ctrl_btn-"+list[i]).value=="1"){
				localStorage.setItem("time-"+list[i],document.getElementById("time-"+list[i]).placeholder);
			}
		}
	}
    changeIcon(checkStatus());//обновим иконк
}

//----------------------------------------------END LISTENERS-----

//TIMERS------------------------------------------------------
window.setInterval(updateTime,1000);
//var btn = document.getElementById("ctrl_btn");

//----------------------------------------------END TIMERS-----